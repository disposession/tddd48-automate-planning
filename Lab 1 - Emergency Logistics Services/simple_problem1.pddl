(define (problem onetoone)
    (:domain esl)
    (:objects
        helicopter1 - helicopter
        depot apertureLab - location
        companionCube - crate
        science - item
        chell - person
    )

    (:init
        (contains companionCube science)
        (located helicopter1 depot)
        (located companionCube depot)
        (located chell apertureLab)
        (free helicopter1)
    )

    (:goal 
        (and 
            (located helicopter1 depot)
            (has chell science)
        )
    )
)
