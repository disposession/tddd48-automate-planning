;; Emergency Services Logistics - Initial Version
(define (domain esl)
	(:requirements :strips :typing)
	(:types
		;; Containers, helicopters and people have locations
		;; Crates can only be picked up from depots, however, isn̈́t that a location
		;; specific from the problem instantiation
		location item carrier positioned - object 
		person crate helicopter - positioned
	)
	(:predicates
		;; Static predicates (None)
		
		;; Non-static predicates
		(contains ?c - crate ?i - item)
		(located ?x - positioned ?l - location)
		(has ?p - person ?i - item)
		(holding ?h - helicopter ?c - crate)
		(free ?h - helicopter)
	)
	(:action
		;; Helicopter grab crate
		grab
		:parameters(?c - crate ?h - helicopter ?l - location)
		:precondition (and (located ?h ?l) (located ?c ?l) (free ?h))
		:effect (and (not (located ?c ?l)) (holding ?h ?c) (not (free ?h)))
  	)
	(:action
		;; Helicopter drop crate c
		drop
		:parameters(?c - crate ?h - helicopter ?l - location ?p - person ?i - item)
		:precondition (and (located ?h ?l) (holding ?h ?c) (contains ?c ?i) (located ?p ?l))
		:effect (and (located ?c ?l) (not (holding ?h ?c)) (free ?h) (has ?p ?i) (not (contains ?c ?i)))
  	)
	(:action
		;; Helicopter move between locations
		move
		:parameters(?h - helicopter ?l1 - location ?l2 - location)
		:precondition (located ?h ?l1)
		:effect (and (not(located ?h ?l1)) (located ?h ?l2))
  	)
)
