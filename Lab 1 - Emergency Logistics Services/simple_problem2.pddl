(define (problem twotothree)
    (:domain esl)
    (:objects
        depot hospital shelter - location
        doctor master - person
        vortexContainer1 vortexContainer2 vortexContainer3 - crate
        sonicScrewdriver psychicPaper potatoes - item
        tardis - helicopter
    )

    (:init
        (contains vortexContainer1 sonicScrewdriver)
        (contains vortexContainer2 psychicPaper)
        (contains vortexContainer3 potatoes)
        (located tardis depot)
        (located vortexContainer1 depot)
        (located vortexContainer2 depot)
        (located vortexContainer3 depot)
        (located doctor hospital)
        (located master shelter)
        (free tardis)
    )

    (:goal (and (has doctor sonicScrewdriver) (has master psychicPaper)))
)
