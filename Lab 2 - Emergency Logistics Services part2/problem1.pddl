(define (problem onetoone)
    (:domain esl)
    (:objects
        helicopter1 - helicopter
        depot apertureLab - location
        companionCube - crate
        GLaDOS - carrier
        science - item
        chell - person
        n0 n1 n2 n3 n4 - num
    )

    (:init
        (contains companionCube science)
        (located helicopter1 depot)
        (located GLaDOS depot)
        (located companionCube depot)
        (located chell apertureLab)
        (free helicopter1)
        (carrying GLaDOS n0)
        (next n0 n1) (next n1 n2) (next n2 n3) (next n3 n4)
    )

    (:goal 
        (and 
            (located helicopter1 depot)
            (has chell science)
        )
    )
)
