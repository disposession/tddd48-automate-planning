(define (problem twotothree)
    (:domain esl)
    (:objects
        depot hospital shelter - location
        doctor master - person
        vortexContainer1 vortexContainer2 vortexContainer3 - crate
        grolax - carrier
        sonicScrewdriver psychicPaper potatoes - item
        tardis - helicopter
        n0 n1 n2 n3 n4 - num
    )

    (:init
        (contains vortexContainer1 sonicScrewdriver)
        (contains vortexContainer2 psychicPaper)
        (contains vortexContainer3 potatoes)
        (located tardis depot)
        (located grolax depot)
        (located vortexContainer1 depot)
        (located vortexContainer2 depot)
        (located vortexContainer3 depot)
        (located doctor hospital)
        (located master shelter)
        (carrying grolax n0)
        (free tardis)
        (next n0 n1) (next n1 n2) (next n2 n3) (next n3 n4)
    )

    (:goal (and (has doctor sonicScrewdriver) (has doctor potatoes) (has master psychicPaper) (located tardis depot)))
)
