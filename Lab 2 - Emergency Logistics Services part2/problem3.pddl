(define (problem twotothree)
    (:domain esl)
    (:objects
        depot hospital - location
        p1 p2 p3 p4 - person
        c1 c2 c3 c4 - crate
        grolax - carrier
        sonicScrewdriver psychicPaper potatoes - item
        tardis - helicopter
        n0 n1 n2 n3 n4 - num
    )

    (:init
        (contains c1 sonicScrewdriver)
        (contains c2 psychicPaper)
        (contains c3 potatoes)
        (contains c4 potatoes)
        (located tardis depot)
        (located grolax depot)
        (located c1 depot)
        (located c2 depot)
        (located c3 depot)
        (located c4 depot)
        (located p1 hospital)
        (located p2 hospital)
        (located p3 hospital)
        (located p4 hospital)
        (carrying grolax n0)
        (free tardis)
        (next n0 n1) (next n1 n2) (next n2 n3) (next n3 n4)
    )

    (:goal (and (has p1 sonicScrewdriver) (has p2 potatoes) (has p3 psychicPaper) (has p4 potatoes) (located tardis depot)))
)
