;; Emergency Services Logistics - Initial Version
(define (domain esl)
	;;(:requirements :strips :typing)
	(:requirements :strips :typing :action-costs)
	(:types
		;; Containers, helicopters and people have locations
		;; Crates can only be picked up from depots, however, isn̈́t that a location
		;; specific from the problem instantiation
		location item positioned num - object 
		crate carrier - carriable
		person helicopter carriable - positioned
	)

	(:predicates
		;; Static predicates (None)
		
		;; Non-static predicates
		(contains ?c - crate ?i - item)
		(in ?c - crate ?cr - carrier)
		(located ?x - positioned ?l - location)
		(has ?p - person ?i - item)
		(holding ?h - helicopter ?c - carriable)
		(free ?h - helicopter)
		(carrying ?cr - carrier ?n - num)
		(next ?numA - num ?numB - num)
	)
	(:functions (total-cost) - number
	            (fly-cost ?from ?to - location) - number)
	(:action
		;; Helicopter grabs a carriable (crate or carrier)
		grab
		:parameters(?cr - carriable ?h - helicopter ?l - location)
		:precondition (and (located ?h ?l) (located ?cr ?l) (free ?h))
		;;:effect (and (not (located ?cr ?l)) (holding ?h ?cr) (not (free ?h)))
		:effect (and (not (located ?cr ?l)) (holding ?h ?cr) (not (free ?h)) (increase (total-cost) 2))
  	)
  	(:action
		;; Helicopter drop a carriable (crate or carrier)
		drop
		:parameters(?cr - carriable ?h - helicopter ?l - location)
		:precondition (and (located ?h ?l) (holding ?h ?cr))
		;;:effect (and (located ?cr ?l) (not (holding ?h ?cr)) (free ?h))
		:effect (and (located ?cr ?l) (not (holding ?h ?cr)) (free ?h) (increase (total-cost) 2))
  	)
  	(:action
		;; Helicopter drop carrier
		give
		:parameters(?c - crate ?h - helicopter ?l - location ?i - item ?p - person)
		:precondition (and (located ?h ?l) (located ?c ?l) (located ?p ?l) (free ?h) (contains ?c ?i))
		;;:effect (and (has ?p ?i) (not (contains ?c ?i)))
		:effect (and (has ?p ?i) (not (contains ?c ?i)) (increase (total-cost) 1))
  	)
  	(:action
		;; Helicopter loads crate to carrier
		load
		:parameters(?c - crate ?h - helicopter ?cr - carrier ?l - location ?now ?next - num)
		:precondition (and (located ?h ?l) (located ?cr ?l) (located ?c ?l) (free ?h) (carrying ?cr ?now) (next ?now ?next))
		;;:effect (and (not (located ?c ?l)) (in ?c ?cr) (not (carrying ?cr ?now)) (carrying ?cr ?next))
		:effect (and (not (located ?c ?l)) (in ?c ?cr) (not (carrying ?cr ?now)) (carrying ?cr ?next) (increase (total-cost) 2))
  	)
	(:action
		;; Helicopter unloads crate from carrier
		unload
		:parameters(?c - crate ?h - helicopter ?cr - carrier ?l - location ?now ?next - num)
		:precondition (and (located ?h ?l) (located ?cr ?l) (in ?c ?cr) (free ?h) (carrying ?cr ?now) (next ?next ?now))
		;;:effect (and (located ?c ?l) (not (in ?c ?cr)) (not (carrying ?cr ?now)) (carrying ?cr  ?next))
		:effect (and (located ?c ?l) (not (in ?c ?cr)) (not (carrying ?cr ?now)) (carrying ?cr  ?next) (increase (total-cost) 2))
  	)
	(:action
		;; Helicopter move between locations
		move
		:parameters(?h - helicopter ?l1 - location ?l2 - location)
		:precondition (located ?h ?l1)
		;;:effect (and (not(located ?h ?l1)) (located ?h ?l2))
		:effect (and (not(located ?h ?l1)) (located ?h ?l2) (increase (total-cost) (fly-cost ?l1 ?l2)))
  	)
)
